module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '3acc32dc5a9c85049395f7b8c4adc52e'),
  },
});
