'use strict';

/**
 *  additionalsale controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::additionalsale.additionalsale');
