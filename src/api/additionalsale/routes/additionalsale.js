'use strict';

/**
 * additionalsale router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::additionalsale.additionalsale');
