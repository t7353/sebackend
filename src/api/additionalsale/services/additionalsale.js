'use strict';

/**
 * additionalsale service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::additionalsale.additionalsale');
